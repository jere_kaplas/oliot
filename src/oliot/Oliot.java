package oliot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author Jere Kaplas, olio-ohjelmointi, 18.9.2014
 */

public class Oliot {

    public static void main(String[] args) throws IOException {
        
        String name;
        String message;
        
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        
        System.out.print("Anna koiralle nimi: ");
        name = br.readLine();

        Dog d1 = new Dog(name);
        
        System.out.print("Mitä koira sanoo: ");
        message = br.readLine();
    
        d1.speak(message);
    }
    
}
